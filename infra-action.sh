#!/usr/bin/env bash
#
# infra-action.sh <OPERATION>
# 
# perform infra action <OPERATION>
# * shell                     interactive shell
# * cloud-connect             connect to the cloud
# * infra-deploy              deploy the infrastructure
# * infra-destroy             destroy the infrastructure

OPERATION="${1:-"shell"}"
SCRIPT_DIR=$(dirname $(readlink -f $0))

source "${SCRIPT_DIR}/ci/toolset-container/lib.sh.inc"

CONTAINER_RUNTIME_EXE=$(get_container_engine)
CONTAINER_IMAGE="${CONTAINER_IMAGE:-"registry.gitlab.ics.muni.cz:443/cloud/terraform/modules/dask-distributed-2t-infra:1"}"
REPO_NAME=dask-distributed-2t-infra

ADDITIONAL_ARGS=""
if [ "${HOME}/.ssh/id_rsa.pub" ]; then
    ADDITIONAL_ARGS="-v ${HOME}/.ssh/id_rsa.pub:${WORKDIR}/${REPO_NAME}/example/id_rsa.pub:ro"
fi

if [[ "${OPERATION}" =~ ^(shell|cloud-connect|infra-deploy|infra-destroy)$ ]]; then
    ${CONTAINER_RUNTIME_EXE} run -it -v "$PWD:${WORKDIR}/${REPO_NAME}" ${ADDITIONAL_ARGS} "${CONTAINER_IMAGE}" "${OPERATION}"
elif [ "${OPERATION}" == "-h" -o "${OPERATION}" == "--help" ]; then
    awk 'NR>1{if ($1=="#"){$1=" ";print $0}else{exit(0)}}' $0
else
    logerr "ERROR: Invalid operation (${OPERATION})"
    exit 2
fi
