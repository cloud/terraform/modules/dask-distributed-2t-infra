#!/usr/bin/env bash
# Get general project versions
# Usage: general-versions.sh <specific-version> [additional-general-version]
#
# Example usage:
# * general-versions.sh 1.2.0 latest
# 1
# 1.2
# latest

SPECIFIC_VERSION="$1"

[ -n "${SPECIFIC_VERSION}" ] || \
  exit 2

shift

# major version
echo "${SPECIFIC_VERSION}" | grep -Eo '^[0-9]+'
# major + major.minor versions
echo "${SPECIFIC_VERSION}" | grep -Eo '^[0-9]+.[0-9]+'

for i_arg in "$@"; do
    echo "$i_arg"
done
