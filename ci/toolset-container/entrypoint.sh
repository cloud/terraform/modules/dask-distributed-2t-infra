#!/usr/bin/env bash
#
# entrypoint.sh <OPERATION>
# 
# container entrypoint performs operation <OPERATION>
# * shell                     interactive shell
# * cloud-connect             connect to the cloud
# * infra-deploy              deploy the infrastructure
# * infra-destroy             destroy the infrastructure

OPERATION="${1:-"shell"}"

SCRIPT_DIR=$(dirname $(readlink -f $0))

source "${SCRIPT_DIR}/lib.sh.inc"

cd "${WORKDIR}/dask-distributed-2t-infra/example"

if [ "${OPERATION}" == "shell" ]; then
    bash
elif [ "${OPERATION}" == "cloud-connect" ]; then
    source_if_exists ${OPENSTACK_RC_FILES}
    ostack_cloud_connect

elif [ "${OPERATION}" == "infra-deploy" -o "${OPERATION}" == "infra-destroy" ]; then
    generate_ssh_keypair_if_missing id_rsa
    source_if_exists ${OPENSTACK_RC_FILES}
    ostack_cloud_connect
    project_type=group
    tf_variable_file=g1-prod-brno-group-projects.tfvars
    if project_name=$(is_personal_project); then
        project_type=personal
        tf_variable_file=g1-prod-brno-personal-projects.tfvars
    fi
    terraform init
    terraform validate
    if [ "${OPERATION}" == "infra-destroy" ]; then
        terraform destroy -var-file ${tf_variable_file}
    else
        terraform plan -var-file ${tf_variable_file} --out plan
        terraform apply plan
    fi
else
    logerr "ERROR: Invalid operation (${OPERATION})"
    exit 2
fi
