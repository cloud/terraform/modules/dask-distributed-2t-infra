

#############################################################################
# variables
#############################################################################
OPENSTACK_BIN="${OPENSTACK_BIN:-openstack}"
OPENSTACK_RC_FILES="${OPENSTACK_RC_FILES:-"./project-openrc.sh ../project-openrc.sh"}"
WORKDIR="/workdir"

[[ "${DEBUG}" =~ ^1|true|True$ ]] && set -x

#############################################################################
# functions
#############################################################################

function log() {
    echo -e "$@"
}

function logerr() {
    echo -e "$@" >&2
}

function get_container_engine() {
    local container_engine_clients="podman docker nerdctl"
    for i_container_engine_client in $container_engine_clients; do
        if $i_container_engine_client info &>/dev/null; then
            echo "$i_container_engine_client"
            return 0
        fi
    done
    return 1
}

function source_if_exists() {
    for i_file in "$@"; do
        if [ -f "${i_file}" -a -r "${i_file}" ]; then
            source "${i_file}"
            log "INFO: ${i_file} successfully loaded."
        else
            logerr "WARNING: ${i_file} not found."
        fi
    done
}

function ostack_cloud_connect() {
    if reply=$(${OPENSTACK_BIN} version show -fcsv); then
        echo "${reply}" | grep identity
    else
        return 1
    fi
}


function is_personal_project() {
    if [ -n "${OS_APPLICATION_CREDENTIAL_ID}" ]; then
        local project_id="$(${OPENSTACK_BIN} application credential show ${OS_APPLICATION_CREDENTIAL_ID} -f value -c project_id)"
        local user_id="$(${OPENSTACK_BIN} application credential show ${OS_APPLICATION_CREDENTIAL_ID} -f value -c user_id)"
        local project_name="$(${OPENSTACK_BIN} project show "${project_id}" -fvalue -c name)"
        local user_name="$(${OPENSTACK_BIN} user show "${user_id}" -fvalue -c name)"
        echo "${project_name}"
        [[ "${project_name}" == "${user_name}" && "${user_name}" =~ [a-fA-F0-9]+@[a-z.]+ ]]
    elif [ -n "${OS_USERNAME}" -a -n "${OS_PROJECT_NAME}" ]; then
        echo "${OS_PROJECT_NAME}"
        [[ "${OS_PROJECT_NAME}" == "${OS_USERNAME}" && "${OS_USERNAME}" =~ [a-fA-F0-9]+@[a-z.]+ ]]
    else
        return 2
    fi
}

function generate_ssh_keypair_if_missing() {
    local ssh_private_key_file="$1"
  
    if [ ! -s "${ssh_private_key_file}.pub" ]; then
        ssh-keygen -t rsa -b 4096 -f "${ssh_private_key_file}"
    fi
}

