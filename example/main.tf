terraform {
  backend "local" {}
}

module "toplevel" {
  # terraform module referencing
  # a] locally (useful in case you want to do updates in the TF module itself or for debugging)
  source = "../modules/2tier_public_bastion_private_vm_farm"
  # b] remotely (when you want to use TF module as-is)
  #source                  = "git::https://gitlab.ics.muni.cz/cloud/terraform/modules/dask-distributed-2t-infra//modules/2tier_public_bastion_private_vm_farm"

  infra_name   = "dask-distributed-2t-demo"
  nodes_count  = 2
  nodes_image  = "ubuntu-jammy-x86_64"
  nodes_flavor = "standard.medium"

  # Externally defined parameters (see *.tfvar files)
  # -------------------------------------------------------------------------
  # root variables wired 1:1 to "toplevel" module to be able to toggle between
  # group and personal project infrastructure in various clouds
  internal_network_creation_enable = var.internal_network_creation_enable
  internal_network_name            = var.internal_network_name
  internal_subnet_creation_enable  = var.internal_subnet_creation_enable
  internal_subnet_name             = var.internal_subnet_name
  router_creation_enable           = var.router_creation_enable
  public_external_network          = var.public_external_network
  public_external_network_ipv6     = var.public_external_network_ipv6
}
