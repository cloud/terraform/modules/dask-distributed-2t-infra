#!/usr/bin/env bash

SCRIPT_DIR=$(dirname $(readlink -f $0))

# IPv6 is enabled but not as default
sudo sysctl -w net.ipv6.conf.default.disable_ipv6=1

${SCRIPT_DIR}/install-pkgs.sh ${SCRIPT_DIR}/requirements.apt
${SCRIPT_DIR}/install-pymodules.sh ${SCRIPT_DIR}/requirements.pip

# launch the scheduler on the bg
while true; do
  dask-scheduler
  sleep 1
done &> /tmp/dask-scheduler.log &

sleep 1m
# submit jobs
while true; do
  python3 ${SCRIPT_DIR}/numpy-experiments.py
  sleep 1m
done
