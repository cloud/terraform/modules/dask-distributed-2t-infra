#!/usr/bin/env bash

DASK_SCHEDULER_HOST="${1}"
DASK_SCHEDULER_PORT="${2:-"8786"}"

SCRIPT_DIR=$(dirname $(readlink -f $0))

# IPv6 is enabled but not as default
sudo sysctl -w net.ipv6.conf.default.disable_ipv6=1

${SCRIPT_DIR}/install-pkgs.sh ${SCRIPT_DIR}/requirements.apt
${SCRIPT_DIR}/install-pymodules.sh ${SCRIPT_DIR}/requirements.pip

while true; do
  dask-worker ${DASK_SCHEDULER_HOST}:${DASK_SCHEDULER_PORT}
  sleep 1
done

