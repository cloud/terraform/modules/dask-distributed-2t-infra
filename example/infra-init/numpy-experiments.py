import math
import random

import numpy
from dask.distributed import Client

# Define the scientific experiment function
def experiment_square(x=42):
    # Example: calculate the square of the input number
    return numpy.square(x)

def experiment_prime1(upto=100000):
    return filter(lambda num: (num % numpy.arange(2,1+int(math.sqrt(num)))).all(), range(2,upto+1))

def experiment_prime2(upto=100000):
    return filter(lambda num: numpy.array([num % factor for factor in range(2,1+int(math.sqrt(num)))]).all(), range(2,upto+1))

def _experiment_prime3(upto=100000):
    return [2]+filter(lambda num: (num % numpy.arange(3,1+int(math.sqrt(num)),2)).all(), range(3,upto+1,2))

def experiment_prime4(upto=100000):
    primes=[2]
    for num in range(3,upto+1,2):
        isprime=True
        for factor in range(3,1+int(math.sqrt(num)),2):
             if not num % factor: isprime=False; break
        if isprime: primes.append(num)
    return primes

def experiment_prime5(upto=100000):
    primes=numpy.arange(2,upto+1)
    isprime=numpy.ones(upto-1,dtype=bool)
    for factor in primes[:int(math.sqrt(upto))]:
        if isprime[factor-2]: isprime[factor*2-2::factor]=0
    return primes[isprime]

def experiment_prime6(upto=1000000):
    primes=numpy.arange(3,upto+1,2)
    isprime=numpy.ones((upto-1)//2,dtype=bool)
    for factor in primes[:int(math.sqrt(upto))//2]:
        if isprime[(factor-2)//2]: isprime[(factor*3-2)//2::factor]=0
    return numpy.insert(primes[isprime],0,2)

# Connect to the Dask scheduler
client = Client('localhost:8786')  # Replace 'localhost' with the scheduler's IP if needed

experiments = {i_foo_name: i_foo for i_foo_name, i_foo in locals().items() if i_foo_name.startswith('experiment_')}
while True:
    for i_experiment_name in experiments.keys():
        # Submit the task to the worker node
        i_future = client.submit(experiments[i_experiment_name], random.randint(1000, 100000))

        # Get the result from the worker node
        i_result = i_future.result()

        # Display the result
        print(f"Scientific experiment {i_experiment_name} resulted in {i_result}")
