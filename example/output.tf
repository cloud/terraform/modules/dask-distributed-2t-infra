output "bastion_external_ipv4_ip" {
  value = module.toplevel.bastion_external_ipv4_ip
}

output "bastion_internal_ipv4_ip" {
  value = module.toplevel.bastion_internal_ipv4_ip
}

output "bastion_external_ipv6_ip" {
  value = module.toplevel.bastion_external_ipv6_ip
}

