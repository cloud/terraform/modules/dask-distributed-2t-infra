##################################
# Define Network Security Groups #
##################################


resource "openstack_networking_secgroup_v2" "secgroup_default" {
  name        = "${var.infra_name}_security_group"
  description = "${var.infra_name} Security group"
}


# Allow all internal TCP & UDP

/* resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_internal_alltcp4" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 1
  port_range_max    = 65535
  remote_ip_prefix  = var.internal_network_cidr
  security_group_id = openstack_networking_secgroup_v2.secgroup_default.id
}

resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_internal_alludp4" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 1
  port_range_max    = 65535
  remote_ip_prefix  = var.internal_network_cidr
  security_group_id = openstack_networking_secgroup_v2.secgroup_default.id
} */


# Explicit external communication

resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_dask_http_ipv4" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 8786
  port_range_max    = 8787
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.secgroup_default.id
}

resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_dask_http_ipv6" {
  direction         = "ingress"
  ethertype         = "IPv6"
  protocol          = "tcp"
  port_range_min    = 8786
  port_range_max    = 8787
  remote_ip_prefix  = "::/0"
  security_group_id = openstack_networking_secgroup_v2.secgroup_default.id
}


# ICMP

resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_icmp4" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  port_range_min    = 0
  port_range_max    = 0
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.secgroup_default.id
}

resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_icmp6" {
  direction         = "ingress"
  ethertype         = "IPv6"
  protocol          = "ipv6-icmp"
  port_range_min    = 0
  port_range_max    = 0
  remote_ip_prefix  = "::/0"
  security_group_id = openstack_networking_secgroup_v2.secgroup_default.id
}

# SSH

resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_ssh4" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.secgroup_default.id
}

resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_ssh6" {
  direction         = "ingress"
  ethertype         = "IPv6"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "::/0"
  security_group_id = openstack_networking_secgroup_v2.secgroup_default.id
}
