# Floating IPs (only for bastion node)
resource "openstack_networking_floatingip_v2" "bastion_fip" {
  pool = var.public_external_network
}

resource "openstack_networking_floatingip_associate_v2" "bastion_fip_associate" {
  floating_ip = openstack_networking_floatingip_v2.bastion_fip.address
  port_id     = openstack_networking_port_v2.bastion_port.id
}

# ipv4 port
resource "openstack_networking_port_v2" "bastion_port" {
  name               = "${var.infra_name}-${var.bastion_name}-port"
  network_id         = var.internal_network_creation_enable ? openstack_networking_network_v2.network_default[0].id : data.openstack_networking_network_v2.internal_shared_personal_network[0].id
  admin_state_up     = "true"
  security_group_ids = [openstack_networking_secgroup_v2.secgroup_default.id]
  fixed_ip {
    subnet_id = var.internal_subnet_creation_enable ? openstack_networking_subnet_v2.subnet_default[0].id : data.openstack_networking_subnet_v2.internal_shared_personal_subnet[0].id
  }
}

# optional ipv6 port
resource "openstack_networking_port_v2" "bastion_port_ipv6" {
  count              = var.public_external_network_ipv6 != "" ? 1 : 0
  name               = "${var.infra_name}-${var.bastion_name}-port-ipv6"
  network_id         = data.openstack_networking_network_v2.external_network_ipv6[0].id
  admin_state_up     = "true"
  security_group_ids = [openstack_networking_secgroup_v2.secgroup_default.id]
}

resource "openstack_compute_interface_attach_v2" "bastion_port_ipv6_attachment" {
  count       = var.public_external_network_ipv6 != "" ? 1 : 0
  port_id     = openstack_networking_port_v2.bastion_port_ipv6[0].id
  instance_id = openstack_compute_instance_v2.bastion.id
}
