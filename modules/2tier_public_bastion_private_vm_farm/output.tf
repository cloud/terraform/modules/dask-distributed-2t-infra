output "bastion_external_ipv4_ip" {
  value = openstack_networking_floatingip_v2.bastion_fip.address
}

output "bastion_internal_ipv4_ip" {
  value = openstack_networking_port_v2.bastion_port.all_fixed_ips
}

output "bastion_external_ipv6_ip" {
  value = var.public_external_network_ipv6 != "" ? openstack_networking_port_v2.bastion_port_ipv6[0].all_fixed_ips : null
}