resource "openstack_compute_instance_v2" "bastion" {
  name            = "${var.infra_name}-${var.bastion_name}"
  image_name      = var.bastion_image
  flavor_name     = var.bastion_flavor
  key_pair        = openstack_compute_keypair_v2.pubkey.name
  security_groups = [openstack_networking_secgroup_v2.secgroup_default.name]
  user_data = templatefile("${path.cwd}/cloud-init.tftpl", {
    hostname                    = "${var.infra_name}-${var.bastion_name}.local"
    start_infrastructure_script = "bastion-start-infrastructure.sh"
    start_infrastructure_args   = []
  })

  network {
    uuid = var.internal_network_creation_enable ? openstack_networking_network_v2.network_default[0].id : data.openstack_networking_network_v2.internal_shared_personal_network[0].id
    port = openstack_networking_port_v2.bastion_port.id
  }
}
