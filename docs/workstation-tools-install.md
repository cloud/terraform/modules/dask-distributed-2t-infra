# How to install needed tools to your workstation

The repository relies on Git and any container engine.

## On Windows

Install following packages:
* https://docs.docker.com/desktop/install/windows-install/
* https://git-scm.com/download/win or https://podman.io/docs/installation

## Linux Ubuntu / Debian

```console
apt update
apt install git-core podman
```


## Linux RHEL / Fedora / CentOS / SLES

```console
yum install git-core podman
```



