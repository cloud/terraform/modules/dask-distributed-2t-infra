# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## [1.5.0] - 2024-04-29
### Fixed
- CI container listing
- add SSh key pair generation if needed

## [1.4.0] - 2024-04-29
### Fixed
- container entrypoint
### Changed
- new documentation in docs/

## [1.3.0] - 2024-04-29
### Changed
- new container tag
- CI display all tags

## [1.2.0] - 2024-04-29
### Changed
- container automation added (entrypoint)
- repo automation added (infra-action.sh)

## [1.1.0] - 2024-04-27
### Changed
- Automation improved ()
### Fixed
- Terraform infrastructure cloud-init templating


## [1.0.0] - 2024-04-26
### Added
- Initial release
