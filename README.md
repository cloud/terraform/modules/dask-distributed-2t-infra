# dask-distributed-2t-infra

IaaS OpenStack cloud dask example infrastructure.


## LGTM

This repository shows declaative, scalable, reproducible a delegable IaaS OpenStack cloud infrastructure.

[Read related presentation](docs/OpenStack-cloud-efficiently-with-Terraform.pdf).

## Architecture

[DASK infrastructure](https://www.dask.org/) consist of 
* single bastion / jump VM (acting as DASK scheduler and dashboard)
* multiple (2) DASK workers

![](/docs/pictures/two-tier-infra.png)

## How to deploy the IaaS infrastructure

1. [Install the minimum workstation requirements](docs/workstation-tools-install.md)
1. Get OpenStack openrc file (store as `project-openrc.sh`)
1. Clone this repository
1. Tune the desired infrastructure in [/example](/example) dir.
1. Test cloud connection with `./infra-action.sh cloud-connect`
1. Deploy with `./infra-action.sh infra-deploy`

### Deployment logs

Are you having problem running the code? See reference logs located in [logs/ dir](/logs/).
